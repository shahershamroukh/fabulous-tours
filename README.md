
![](https://img.shields.io/badge/Fabuloustours-blueviolet)

# Fabulous-tours

> This project is for a travel agency specialized in outdoors trips and adventures the project built entirely  with HTML, CSS, and SASS.


![screenshot](./img/fabulous.png)

## Built With

- HTML
- CSS
- SASS

## Live Demo

[Live Demo Link](https://fabuloustours.netlify.app/)


## Authors

👤 **Author**

- GitHub: [@githubhandle](https://github.com/Shaher-11)
- Twitter: [@twitterhandle](https://twitter.com/ShaherShamroukh/)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/shaher-shamroukh/)

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Jonas Schmedtmann
- CSS tricks

## 📝 License

This project is [MIT](lic.url) licensed.
